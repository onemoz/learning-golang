package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func fib(n int) int {

	if n < 0 {
		log.Fatal("Can't calculate negative index of fib serie")
		os.Exit(1)
	}

	switch n {
	case 0:
		return 0
	case 1:
		return 1
	default:
		return fib(n-1) + fib(n-2)
	}
}

func main() {

	fmt.Println("calculate fib(n). Enter n:")
	reader := bufio.NewReader(os.Stdin)
	n, err := reader.ReadString('\n')

	if err != nil {
		log.Fatal("Failed to read line", err)
	}

	value, err := strconv.ParseInt(strings.Trim(n, "\n \t\r"), 10, 64)

	if err != nil {
		log.Fatal("Cannot convert ", n, " to int type: ", err)
	}

	if value < 0 {
		log.Fatal("n must be > 0")
	}

	fmt.Println("result = ", fib(int(value)))

}
