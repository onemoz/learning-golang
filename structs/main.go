package main

import "fmt"

type contactInfo struct {
	email   string
	zipCode int
}

type person struct {
	firstName string
	lastName  string
	contactInfo
}

func (p person) print() {
	fmt.Printf("%v", p)
}

func (p *person) updateName(newFirstName string) {
	p.firstName = newFirstName
}

func main() {

	//alex := person{firstNane: "Alex", lastName: "Anderson"}
	//var alex person
	//alex := person("Alex", "Anderson", "test@email.com", 30210)
	jim := person{
		firstName: "Jim",
		lastName:  "Party",
		contactInfo: contactInfo{
			email:   "jim@gmail.com",
			zipCode: 94000,
		},
	}

	jim.print()

	jim.updateName("Gordon")
	jim.print()
}
