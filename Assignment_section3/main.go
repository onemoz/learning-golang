package main

import "fmt"

func main() {

	suite := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	for _, number := range suite {

		if suite[number]%2 == 0 {
			fmt.Println(suite[number], "is even")
		} else {
			fmt.Println(suite[number], "is odd")
		}
	}
}
