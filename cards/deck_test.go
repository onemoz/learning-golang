package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()
	if len(d) != 16 {
		t.Errorf("Expected deck length of 12 but got %v", len(d))
	}

	if d[0] != "Ace of Spades" {
		t.Errorf("Expected first card to be Ace of Spades but got %v", d[0])

	}
	if d[len(d)-1] != "Four of Clubs" {
		t.Errorf("Expected last card to be Four of Clubs but got %v", d[len(d)-1])

	}
}

func TestShuffle(t *testing.T) {

	d := newDeck()
	d.shuffle()

	if len(d) != 16 {
		t.Errorf("Expected deck length of 16 but got %v", len(d))
	}

	if d[0] == "Ace of Spades" {
		t.Errorf("Expected first card to not be Ace of Spades but got %v", d[0])

	}
	if d[len(d)-1] == "Four of Clubs" {
		t.Errorf("Expected last card to not be Four of Clubs but got %v", d[len(d)-1])

	}
}

func TestSaveToDeckAndNewDeckFromFile(t *testing.T) {

	os.RemoveAll("_decktesting")

	deck := newDeck()

	err := deck.saveToFile("_decktesting")
	if err != nil {
		t.Errorf("Couldn't save the deck to file due to error %v", err)
	}

	loadedDeck := newDeckFromFile("_decktesting")
	if err != nil {
		t.Errorf("Couldn't load the deck from file due to error %v", err)
	}

	if len(loadedDeck) != 16 {
		t.Errorf("Expected deck length of 16 but got %v", len(loadedDeck))

	}
	os.RemoveAll("_decktesting")
}
