package main

func main() {
	//cards := newDeck()

	// cards.saveToFile("my_cards")
	cards := newDeckFromFile("my_cards")

	cards.print()
	cards.shuffleIntn()
	cards.print()

	// hand, remainingDeck := deal(cards, 3)
	// hand.print()
	// remainingDeck.print()
}
