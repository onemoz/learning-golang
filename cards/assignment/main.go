package main

func even(i int) bool {

	if i%2 == 0 {
		return true
	} else {
		return false
	}
}

func main() {

	slice := []int{}

	for i := 0; i < 11; i++ {
		slice = append(slice, i)
	}

	for i := range slice {
		if even(slice[i]) {
			println(slice[i], "is even")
		} else {
			println(slice[i], "is odd")

		}
	}
}
