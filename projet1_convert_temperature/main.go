package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {

	fmt.Println("Enter Value:")
	reader := bufio.NewReader(os.Stdin)
	valueString, err := reader.ReadString('\n')

	if err != nil {
		log.Fatal("error while reading keyboard input", err)

	}

	// converting the input from keyboard from string to float
	value, err := strconv.ParseFloat(strings.Trim(valueString, "\t \r\n"), 64)

	if err != nil {
		log.Fatal("error while while converting from String to Int", err)
		os.Exit(1)
	}

	fmt.Println("convert to C or F? [c|f]")
	readerUnit := bufio.NewReader(os.Stdin)

	unit, err := readerUnit.ReadString('\n')

	if err != nil {
		log.Fatal("error while reading keyboard input", err)
		os.Exit(1)
	}

	switch strings.Trim(unit, "\t \r\n") {
	case "c":
		fmt.Println((value-32)/1.8, " celsius")
	case "f":
		fmt.Println((value*1.8)+32, " farenheit")
	default:
		fmt.Println("The input isn't 'c' or 'f'")
	}

}
