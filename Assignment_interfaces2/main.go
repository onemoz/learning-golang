package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func main() {

	s := os.Args

	file, err := os.Open(s[1])

	if err != nil {
		log.Fatal("Error while reading file: ", err)
	}

	written, err := io.Copy(os.Stdout, file)

	if err != nil {
		log.Fatal("Error copying file to stdout: ", err)
	}

	fmt.Println("Written bytes: ", written)
}
