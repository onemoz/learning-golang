package main

import "fmt"

func main() {

	c := make(chan string)

	names := []string{"norbert", "paul", "jacques"}

	// fmt.Println(names[0])

	for _, name := range names {
		go makeString(name, c)
	}

	for l := range c {
		fmt.Println(l)

	}
}

func makeString(s string, c chan string) {

	c <- "bonjour " + s
}
