package main

import "fmt"

func printMap(c map[string]string) {

	for color, hex := range c {
		fmt.Println("hex code for", color, "is", hex)
	}
}

func main() {

	colors := map[string]string{

		"red":   "#ff000000",
		"green": "#4bf745",
		"white": "#ffffff",
	}

	printMap(colors)
	// fmt.Println(colors)

}
